﻿/** Global **/
Devtools = {
    autofocus: false,
    eventSource: null,
    interval: null,
};

/** Event Listener **/
document.addEventListener('DOMContentLoaded', function () {
    // vars
    var project_path = getCurrentScript().getAttribute('project_path');
    var project_url = getCurrentScript().getAttribute('project_url');

    // Help
    document.getElementById('help').addEventListener('click', function (e) {
        document.getElementById('help-tooltip').style.display = 'block';
    });

    // Hide help when clicking elsewhere
    document.addEventListener('click', function (e) {
        if (e.target.id !== 'help') {
            document.getElementById('help-tooltip').style.display = 'none';
        }
    });

    // Sort
    sortTable('desc');
    document.getElementById('sort').addEventListener('click', function (e) {
        var i = this.querySelector('i');
        if (i.classList.contains('fa-sort-desc')) {
            i.classList.remove('fa-sort-desc');
            i.classList.add('fa-sort-asc');
            sortTable('asc');
        }
        else {
            i.classList.remove('fa-sort-asc');
            i.classList.add('fa-sort-desc');
            sortTable('desc');
        }
    });

    // Filter
    document.getElementById('filter-request').addEventListener('change', function (e) {
        detailClose();
        filterTable();
    });

    // Clear cache
    document.getElementById('clear-cache').addEventListener('click', function (e) {
        //animation
        this.classList.remove("animated");
        void this.offsetWidth;
        this.classList.add('animated');

        var clear_cache_url = getCurrentScript().getAttribute('clear_cache_url')
        get(clear_cache_url, function (e) {
            // Succes
            console.log("Success clear cache Mediboard");
        }, function () {
            console.error("Error clear cache Mediboard");
        });
    });

    // Détail
    document.getElementById('detail-close').addEventListener('click', function (e) {
        detailClose();
    });

    // Load request
    document.getElementById('refresh').addEventListener('click', function (e) {
        //animation
        var i = this.querySelector('i');
        i.classList.add('fa-spin');
        setTimeout(function () {
            i.classList.remove('fa-spin');
        }, 500);

        var table = document.getElementById('request-table');
        var rows = table.getElementsByTagName('tr');
        var request_ids = [];

        for (let row of rows) {
            if (row.getAttribute('request_id')) {
                request_ids.push(row.getAttribute('request_id'));
            }
        }
        request_ids_json = JSON.stringify(request_ids);

        var new_requests = [];
        get(project_path + '/index.php?action=list', function (json) {
            try {
                new_requests = JSON.parse(json);
            } catch (e) {
                console.error("Parsing error:", e);
            }

            new_requests.forEach(function (request_id) {
                get(project_url + '/' + request_id + ".json", function (json) {
                    var objson = JSON.parse(json);
                    objson.file_html = project_url + '/' + request_id + ".html";
                    addRowRequest(objson);
                });
            });

        }, null, request_ids_json);
    });
    document.getElementById('refresh').click();

    // Clear request
    document.getElementById('clear-request').addEventListener('click', function (e) {
        var table = document.getElementById("request-table");
        var rows = table.getElementsByTagName('tr');
        var count = rows.length;
        for (var x = count - 1; x > 0; x--) {
            if (rows[x].classList[0] !== 'no-delete') {
                table.deleteRow(x);
            }
        }
        document.getElementById("request-count").innerHTML = "0 Request";
        document.getElementById('detail-top').style.display = 'none';
        document.getElementById('detail').style.display = 'none';
        document.getElementById('request-table').classList.remove('small');
        get(project_path + '/index.php?action=clear', function (json) {
            // Succes
            try {
                var objson = JSON.parse(json);
                console.log(objson);
            } catch (e) {
                console.error("Parsing error:", e);
            }
        }, function () {
            // Error
        });
    });

    // Toggle Autofocus on last request
    var autofocus_toggle = document.getElementById('autofocus-last');
    autofocus_toggle.addEventListener('click', function (e) {
        var i = this.querySelector('i');
        if (i.classList.contains('fa-eye-slash')) {
            this.classList.add('tomato');
            i.classList.remove('fa-eye-slash');
            i.classList.add('fa-eye');
        }
        else {
            this.classList.remove('tomato');
            i.classList.remove('fa-eye');
            i.classList.add('fa-eye-slash');
        }

        autofocus_toggle.classList.toggle('active');
        Devtools.autofocus = !Devtools.autofocus;
    });

    // auto refresh
    document.getElementById('auto-refresh').addEventListener('click', function (e) {
        if (!this.classList.contains('tomato')) {
            this.classList.add('tomato');
            Devtools.interval = window.setInterval(function () {
                document.getElementById('refresh').click();
            }, 5000);
        }
        else {
            this.classList.remove('tomato');
            window.clearInterval(Devtools.interval);
        }
    });

    // Sidenav
    var links = document.querySelectorAll("#sidenav a");
    [].forEach.call(links, (a) => {
        a.addEventListener('click', function (e) {
            closeSideNavContent();
            a.classList.add("sidenav-active");
            var id = a.id + "-content";
            document.getElementById(id).style.display = "block";
        });
    });

    // Filtres traces
    var filtres = document.querySelectorAll("#traces-filter span");
    [].forEach.call(filtres, (span) => {
        span.addEventListener('click', function (e) {
            if (span.classList.contains("active")) {
                span.classList.remove("active");
            }
            else {
                span.classList.add("active")
            }
            filterDiv("traces", false);
        });
    });

    // Filtres query
    var filtres = document.querySelectorAll("#query-filter span");
    [].forEach.call(filtres, (span) => {
        span.addEventListener('click', function (e) {
            if (span.classList.contains("active")) {
                span.classList.remove("active");
            }
            else {
                span.classList.add("active")
            }
            filterDiv("query", false);
        });
    });

    // var filterall = document.getElementById('toogle-all-filter');
    // filterall.addEventListener('click', function (e) {
    //   if (filterall.classList.contains("fa-check-square")) {
    //     filterall.classList.remove("fa-check-square");
    //     filterall.classList.add("fa-square");
    //     [].forEach.call(filtres, (span) => {
    //       span.classList.remove("active");
    //     });
    //   } else {
    //     filterall.classList.remove("fa-square");
    //     filterall.classList.add("fa-check-square");
    //     [].forEach.call(filtres, (span) => {
    //       span.classList.add("active");
    //     });
    //   }
    //   filterTraces();
    // });

    /** UI STYLE Selector **/
    document.querySelector('#style_selector #mediboard').addEventListener('click', () => {
        const project_url = getCurrentScript().getAttribute('mediboard_url');
        const data = {'m': 'admin', 'dosql': 'do_save_pref', 'key': 'UISTYLE', 'value': 'e-cap', 'ajax': 1};

        const formData = new FormData();
        for (const prop in data) {
            formData.append(prop, data[prop]);
        }

        get(project_url, () => { document.querySelector('#refresh').click(); }, () => {}, formData);
    });
    document.querySelector('#style_selector #tamm').addEventListener('click', () => {
        const project_url = getCurrentScript().getAttribute('mediboard_url');
        const data = {'m': 'admin', 'dosql': 'do_save_pref', 'key': 'UISTYLE', 'value': 'tamm', 'ajax': 1};

        const formData = new FormData();
        for (const prop in data) {
            formData.append(prop, data[prop]);
        }

        get(project_url, () => { document.querySelector('#refresh').click(); }, () => {}, formData);
    });
});

/**
 * Add row in request table
 * @param objson
 */
function addRowRequest(objson) {

    var table = document.getElementById("request-table");
    var count = table.rows.length;

    // add data to table request
    var row = table.insertRow(count - 1);

    row.setAttribute('title', objson.detail.Request.Path);
    row.setAttribute('request_id', objson.request_id);
    row.setAttribute('request_time_float', objson.request_time_float);

    // request
    var cell_0 = row.insertCell(0);
    cell_0.innerHTML = objson.request_uri + "<br><span>" + objson.http_host + "</span>";

    // status
    var cell_1 = row.insertCell(1);
    cell_1.innerHTML = objson.status + "<br><span>" + objson.detail.Response.Status + "</span>";
    if (objson.status >= 400) {
        row.classList.add("request-error");
    }

    // type
    var cell_2 = row.insertCell(2);
    cell_2.innerHTML = objson.detail.Request.Type + "<br><span>" + objson.detail.Response.Content + "</span>";

    // origin
    var cell_3 = row.insertCell(3);
    var ip = objson.detail.Request.Ip;
    var agent = objson.detail.Request.Agent;
    cell_3.innerHTML = ip + "<br><span>" + agent + "</span>";

    // time
    var cell_4 = row.insertCell(4);
    cell_4.innerHTML = objson.detail.Request.Time + "<br><span>" + objson.time + " Sec</span>";

    // size
    var cell_5 = row.insertCell(5);
    cell_5.innerHTML = objson.size + "<br><span>" + objson.memory + "</span>";

    // query
    var cell_6 = row.insertCell(6);
    cell_6.classList.add("td-center");
    cell_6.innerHTML = objson.query_count;

    // error
    var cell_7 = row.insertCell(7);
    cell_7.classList.add("td-center");
    if (objson.error_count > 0) {
        row.classList.add("request-error");
    }
    cell_7.innerHTML = objson.error_count;

    // log
    var cell_8 = row.insertCell(8);
    cell_8.classList.add("td-center");
    cell_8.innerHTML = objson.log_count;

    // dump
    var cell_9 = row.insertCell(9);
    cell_9.classList.add("td-center");
    cell_9.innerHTML = objson.dump_count;

    // Footer & title
    setFooterAndTitle();

    // sort
    if (document.getElementById('sort').querySelector('i').classList.contains("fa-sort-desc")) {
        sortTable('desc');
    }
    if (document.getElementById('sort').querySelector('i').classList.contains("fa-sort-asc")) {
        sortTable('asc');
    }

    // filter
    if (document.getElementById('filter-request').value) {
        filterTable();
    }

    // Event click build detail
    row.addEventListener('click', function (e) {
        if (row.classList.contains('active') === false) {

            // Init
            removeActiveTr();
            row.classList.add('active');
            document.getElementById('detail-top').style.display = 'block';
            document.getElementById('detail').style.display = 'block';
            table.classList.add("small");
            var top = table.offsetTop + "px";
            document.getElementById('detail').style.top = top;
            document.getElementById('detail').style.height = "calc(100% - " + top + ")";

            // Details
            document.getElementById('sidenav-1-content').innerHTML = "";
            if (objson.detail) {
                buildContentJson('sidenav-1-content', objson.detail);
            }

            // Server
            document.getElementById('sidenav-2-content').innerHTML = "";
            if (objson.server) {
                buildContentJson('sidenav-2-content', objson.server);
            }

            // Perf
            document.getElementById('sidenav-3-content').innerHTML = "";
            if (objson.performance) {
                buildContentJson('sidenav-3-content', objson.performance);
            }

            // Traces
            document.getElementById('traces-show').innerHTML = "";
            document.getElementById('traces-count').innerHTML = "";
            document.getElementById('sidenav-4').classList.remove("sidenav-error");

            if (row.classList.contains("request-error")) {
                document.getElementById('sidenav-4').classList.add("sidenav-error");
            }

            // Query
            document.getElementById('query-show').innerHTML = "<div></div>";
            document.getElementById('query-count').innerHTML = "";

            get(objson.file_html, function (html) {
                // Succes
                document.getElementById('traces-show').innerHTML = "<div>" + html + "</div>";
                var div_traces = document.getElementById('traces-show').querySelector('div');
                var div_query = document.getElementById('query-show').querySelector('div');

                // Move queries in another tab (both in html file)
                var divs = document.getElementById('traces-show').querySelectorAll('div');
                [].forEach.call(divs, (div) => {
                    if (div.classList.contains('query') || div.classList.contains('report')) {
                        div_query.appendChild(div.parentNode);
                    }
                });

                // Traces
                document.getElementById('traces-count').innerHTML = div_traces.childElementCount.toString();
                evalSfDump(div_traces);
                filterDiv('traces', true);

                // Queries
                document.getElementById('query-count').innerHTML = div_query.childElementCount.toString();
                evalSfDump(div_query);
                copyToClipboard();
                filterDiv('query', true);

            });
        }
    });

    if (Devtools.autofocus) {
        row.click();
    }
}

function evalSfDump(element) {
    var scripts = element.getElementsByTagName("script");
    for (var i = 0; i < scripts.length; i++) {
        var script = scripts[i].text;
        if (script.indexOf("Sfdump") !== false) {
            eval(script);
        }
    }
}

/**
 * Copy sql query to clipboard
 */
function copyToClipboard() {
    var div = document.getElementById('query-show').querySelector('div');
    div.addEventListener("click", function (e) {
        var srcElement = e.srcElement;
        if (srcElement.tagName === 'PRE') {
            var el = document.createElement('textarea');
            el.value = srcElement.innerText;
            document.body.appendChild(el);
            el.select();
            document.execCommand('copy');
            document.body.removeChild(el);
        }
    });
}

/**
 * Is active listeners
 * @returns {boolean}
 */
function isActive() {
    return document.getElementById("is_active").classList.contains('tomato');
}

/**
 * Set footer info nrb req / avg time
 */
function setFooterAndTitle() {
    var table = document.getElementById("request-table");
    var count = table.rows.length;

    var total = count - 2;

    var html = total + (total > 1 ? " Requests" : " Request");

    var project_title = getCurrentScript().getAttribute('project_title');
    document.title = project_title + " (" + total + ")";

    var rows = table.rows;
    var avg = 0;
    var diviseur = 0;
    var float = 0;
    for (var i = 0; i < table.rows.length; i++) {
        var row = rows[i];
        if (!row.classList.contains('no-delete')) {
            float = parseFloat(row.dataset.time);
            if (!isNaN(float)) {
                avg = avg + float;
                diviseur++;
            }
        }
    }
    if (avg > 0) {
        avg = avg / diviseur;
        html += " | " + avg.toFixed(3) + " Time avg";
    }

    document.getElementById("request-count").innerHTML = html;
}


function filterDiv(type, reset) {
    var filtres = document.querySelectorAll("#" + type + "-filter span");

    // reset
    if (reset) {
        [].forEach.call(filtres, (span) => {
            span.classList.add('active');
        });
        return;
    }

    // filter
    [].forEach.call(filtres, (span) => {
        var active = span.classList.contains('active');
        var classname = span.dataset.classname;
        var divs = document.querySelectorAll("#" + type + "-show > div div." + classname);
        [].forEach.call(divs, (div) => {
            div.parentNode.style.display = active ? 'block' : 'none';
        });
    });
}


/**
 * Build .sidenav-content-json
 * @param selector
 * @param json
 */
function buildContentJson(selector, json) {
    document.getElementById(selector).innerHTML = renderJSON(json);
    var bolds = document.querySelectorAll('#' + selector + ' > div.tree > b');
    [].forEach.call(bolds, (b) => {
        b.classList.remove('hide');
        b.classList.add('show');

        b.addEventListener('click', function (e) {
            // show
            if (b.nextSibling.style.display === 'none') {
                b.nextSibling.style.display = 'block';
                b.classList.remove('hide');
                b.classList.add('show');
            }
            else {
                //hide
                b.nextSibling.style.display = 'none';
                b.classList.remove('show');
                b.classList.add('hide');
            }
        });
    });
}

/**
 * Parse json
 * @param obj
 * @returns {string|string}
 */
function renderJSON(obj) {
    'use strict';
    var keys = [],
        retValue = "";
    for (var key in obj) {
        if (typeof obj[key] === 'object') {
            retValue += "<div class='tree'><b> " + key + '</b><div>';
            retValue += renderJSON(obj[key]);
            retValue += "</div></div>";
        }
        else {
            retValue += "<div class='tree'><b> " + key + "</b> " + obj[key] + "</div>";
        }

        keys.push(key);
    }
    return retValue;
}

/**
 * Invert class and return new value
 * @param element
 * @returns {number}
 */
function toogleRadio(element) {
    var i = element.querySelector('i');
    if (i.classList.contains('fa-check-square')) {
        i.classList.remove('fa-check-square');
        i.classList.add('fa-square');
        return 0;
    }
    else {
        i.classList.remove('fa-square');
        i.classList.add('fa-check-square');
        return 1;
    }
}

/**
 * Row request table
 */
function removeActiveTr() {
    var rows = document.querySelectorAll("#request-table tr");

    [].forEach.call(rows, (row) => {
        row.classList.remove("active");
    });
}


/**
 * Side nav panel event
 */
function closeSideNavContent() {
    var links = document.querySelectorAll("#sidenav a");

    [].forEach.call(links, (a) => {
        a.classList.remove("sidenav-active");
        var id = a.id + "-content";
        document.getElementById(id).style.display = "none";
    });
}

/**
 * Ajax call asynchrone
 * @param url
 * @param functionSucces
 * @param functionError
 */
function get(url, functionSucces, functionError, datas) {
    const req = new XMLHttpRequest();

    req.onreadystatechange = function (event) {
        // XMLHttpRequest.DONE === 4
        if (this.readyState === XMLHttpRequest.DONE) {
            if (req.status === 200) {
                functionSucces(req.responseText);
            }
            else {
                functionError();
            }
        }
    };

    if (datas) {
        req.open('POST', url, true);
        req.send(datas);
    }
    else {
        req.open('GET', url, true);
        req.send();
    }
}


/**
 *
 * @returns {HTMLScriptElement}
 */
function getCurrentScript() {
    var scripts = document.getElementsByTagName('script');
    return scripts[scripts.length - 1];
}

/**
 *
 * @param string order
 */
function sortTable(order) {
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("request-table");
    switching = true;
    /* Make a loop that will continue until
    no switching has been done: */
    while (switching) {
        // Start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /* Loop through all table rows (except the
        first, which contains table headers): */
        for (i = 1; i < (rows.length - 1); i++) {
            // Start by saying there should be no switching:
            shouldSwitch = false;
            /* Get the two elements you want to compare,
            one from current row and one from the next: */
            x = rows[i].getAttribute("request_time_float");
            y = rows[i + 1].getAttribute("request_time_float");

            // Check if the two rows should switch place:
            if (x !== null && x !== undefined && y !== null && y !== undefined) {

                if (order === 'desc' && parseInt(x) < parseInt(y)) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
                if (order === 'asc' && parseInt(x) > parseInt(y)) {
                    // If so, mark as a switch and break the loop:
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            /* If a switch has been marked, make the switch
            and mark that a switch has been done: */
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}

/**
 *
 */
function detailClose() {
    document.getElementById('detail-top').style.display = 'none';
    document.getElementById('detail').style.display = 'none';
    document.getElementById('request-table').classList.remove('small');
    removeActiveTr();
}

/**
 *
 */
function filterTable() {
    // Declare variables
    var input, filter, table, tr, td, i, request_id;
    input = document.getElementById("filter-request");
    filter = input.value;
    table = document.getElementById("request-table");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (i = 0; i < tr.length; i++) {

        if (filter === '') {
            tr[i].style.display = "";
        }
        else {
            request_id = tr[i].getAttribute("request_id");
            if (request_id) {
                tr[i].style.display = request_id === filter ? "" : "none";
            }
        }
    }
}
