<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

namespace OxDevtools;

class Devtools
{
    const PROJECT_TITLE = 'Ox Devtools';

    private $path_to_tmp;
    private $project_path;
    private $url;

    public function __construct()
    {
        $this->path_to_tmp  = dirname(__DIR__, 4) . '/tmp/devtools';
        $this->project_path = './vendor/openxtrem/devtools';
        $this->url          = $this->makeUrl();
    }

    private function makeUrl(): string
    {
         return (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http")
         . "://" . $_SERVER['HTTP_HOST']
         . str_replace('?'.$_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']);
    }

    public function getPathToTmp(): string
    {
        return $this->path_to_tmp;
    }

    /**
     * @return mixed
     */
    public function getProjectPath(): string
    {
        return $this->project_path;
    }

    /**
     * @return mixed
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return mixed
     */
    public function getProjectUrl(): string
    {
        return str_replace('devtools', 'tmp/devtools', $this->url);
    }


    /**
     * @return mixed
     */
    public function getMediboardUrl(): string
    {
        return str_replace('devtools', '', $this->url);;
    }


    /**
     * @return mixed
     */
    public function getClearCacheUrl(): string
    {
        return $this->getMediboardUrl() . 'gui/system/cache/clear?cache=all&target=local&layer=shm';
    }

    public function renderTemplate(): string
    {
        ob_start();
        include_once "./dist/template.html";
        $html = ob_get_contents();
        ob_end_clean();

        return $this->replaceVars($html);
    }

    private function replaceVars($html): string
    {
        $vars = [
            'project_path'    => $this->getProjectPath(),
            'project_url'     => $this->getProjectUrl(),
            'mediboard_url'   => $this->getMediboardUrl(),
            'clear_cache_url' => $this->getClearCacheUrl(),
            'project_title'   => static::PROJECT_TITLE,
        ];

        foreach ($vars as $var => $value) {
            $html = str_replace('{{' . $var . '}}', $value, $html);
        }

        return $html;
    }

    public function listRequest(): string
    {
        $pattern = $this->path_to_tmp . '/*.json';
        $files   = glob($pattern);

        $requests_sended = json_decode(file_get_contents('php://input'));

        $results = [];
        foreach ($files as $file) {
            $request_id = basename($file, '.json');
            if (!in_array($request_id, $requests_sended)) {
                $results[] = $request_id;
            }
        }

        return json_encode($results);
    }

    public function clearRequest(): string
    {
        $pattern = $this->path_to_tmp . '/*.{json,html}';
        $files   = glob($pattern, GLOB_BRACE);

        foreach ($files as $file) {
            unlink($file);
        }

        return json_encode(['count' => count($files)], true);
    }

}
