<?php
/**
 * @author  SAS OpenXtrem <dev@openxtrem.com>
 * @license https://www.gnu.org/licenses/gpl.html GNU General Public License
 * @license https://www.openxtrem.com/licenses/oxol.html OXOL OpenXtrem Open License
 */

use OxDevtools\Devtools;


// Autoload
$autoloadLocations = [
    __DIR__ . '/vendor/autoload.php',
    __DIR__ . '/../../autoload.php',
];

$loaded = false;
foreach ($autoloadLocations as $autoload) {
    if (!$loaded && is_file($autoload)) {
        require_once($autoload);
        $loaded = true;
    }
}

// Instance
$devtools = new Devtools();

// Dispatcher
$action = $_GET['action'] ?? null;
switch ($action) {
    case 'list':
        $response = $devtools->listRequest();
        break;
    case 'clear':
        $response = $devtools->clearRequest();
        break;
    default:
        $response = $devtools->renderTemplate();
        break;
}

// Send
echo $response;

